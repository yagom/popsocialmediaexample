//
//  TimelineTableViewCell.swift
//  POPSocialMedia
//
//  Created by JoSeongGyu on 2016. 7. 8..
//  Copyright © 2016년 yagom. All rights reserved.
//

import UIKit
import AVFoundation

class TimelineTableViewCell: UITableViewCell, MediaContainer {

    lazy var videoLayer: AVPlayerLayer = AVPlayerLayer()
    lazy var mediaImageView: UIImageView = UIImageView()
    
    var note: UILabel = UILabel()
    
    var content: Content? {
        didSet {
            self.contentChanged()
        }
    }
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, content: Content) {
        self.content = content
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentChanged()
        
        let cellHeight: CGFloat = 80
        
        let mediaRect: CGRect = CGRect(x: 5, y: 5, width: cellHeight - 10, height: cellHeight - 10)
        mediaImageView.layout(in: mediaRect)
        videoLayer.layout(in: mediaRect)
        
        mediaImageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        self.addSubview(mediaImageView)
        self.layer.addSublayer(videoLayer)
        
        var noteRect: CGRect = self.bounds
        noteRect.origin.x = mediaRect.size.width + 10
        noteRect.size.width -= noteRect.origin.x
        noteRect.size.height = cellHeight
        
        note.layout(in: noteRect)
        
        
        self.addSubview(note)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
