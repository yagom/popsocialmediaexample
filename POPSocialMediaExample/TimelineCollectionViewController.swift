//
//  TimelineCollectionViewController.swift
//  POPSocialMedia
//
//  Created by JoSeongGyu on 2016. 7. 8..
//  Copyright © 2016년 yagom. All rights reserved.
//

import UIKit

private let reuseIdentifier = "CollectionCell"

class TimelineCollectionViewController: UICollectionViewController, ContainContents, CanShowDetailView {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = timelineTitle
        self.collectionView!.registerClass(TimelineCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfContents
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: TimelineCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! TimelineCollectionViewCell

        cell.content = contents[indexPath.row]
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        showDetailView(contents[indexPath.row])
    }
}
