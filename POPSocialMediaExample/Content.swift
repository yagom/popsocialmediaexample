//
//  Content.swift
//  POPSocialMediaExample
//
//  Created by JoSeongGyu on 2016. 7. 9..
//  Copyright © 2016년 yagom. All rights reserved.
//

import Foundation

struct Content {
    enum MediaType {
        case image, video
    }
    
    var type: Content.MediaType
    var URLString: String
    var note: String
}
