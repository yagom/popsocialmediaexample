//
//  DetailViewController.swift
//  TETETETET
//
//  Created by JoSeongGyu on 2016. 7. 8..
//  Copyright © 2016년 yagom. All rights reserved.
//

import UIKit
import AVFoundation

class DetailViewController: UIViewController, MediaContainer {
    
    lazy var videoLayer: AVPlayerLayer = AVPlayerLayer()
    lazy var mediaImageView: UIImageView = UIImageView()
    
    var note: UILabel = UILabel()
    
    var content: Content? {
        didSet {
            self.contentChanged()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        mediaImageView.layout(in: self.view.bounds)
        videoLayer.layout(in: self.view.bounds)
        
        mediaImageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        self.view.addSubview(mediaImageView)
        self.view.layer.addSublayer(videoLayer)
        
        var noteFrame: CGRect = self.view.bounds
        noteFrame.origin.y = noteFrame.size.height - 50
        noteFrame.size.height = 50
        
        note.layout(in: noteFrame)
        note.textAlignment = .Center
        note.font = UIFont.systemFontOfSize(30)
        self.view.addSubview(note)
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if content?.type == .video {
            self.videoLayer.player?.play()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if content?.type == .video {
            self.videoLayer.player?.pause()
            self.videoLayer.player = nil
        }
    }
}
