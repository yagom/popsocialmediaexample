//
//  CellLayout.swift
//  POPSocialMedia
//
//  Created by JoSeongGyu on 2016. 7. 8..
//  Copyright © 2016년 yagom. All rights reserved.
//

import UIKit
import AVFoundation

// MARK : ContentPresentable

protocol ContentPresentable: class, Layout {
    var canPresentContent: Bool { get }
}

extension ContentPresentable {
    var canPresentContent: Bool {
        return true
    }
}

// MARK : Layout

protocol Layout {
    var frame: CGRect { get set }
    mutating func layout(in rect: CGRect)
}

extension Layout {
    mutating func layout(in rect: CGRect) {
        self.frame = rect
    }
}

extension UIImageView: ContentPresentable { }
extension AVPlayerLayer: ContentPresentable { }
extension UILabel: Layout { }


// MARK : MediaContainer

protocol MediaContainer: class {
    var content: Content? { get set }
    var media: ContentPresentable { get }
    var note: UILabel { get set }
    
    var videoLayer: AVPlayerLayer { get }
    var mediaImageView: UIImageView { get }
    
    func contentChanged()
}

extension MediaContainer {
    func contentChanged() {
        
        guard let content = content else {
            mediaImageView.hidden = true
            videoLayer.hidden = true
            
            mediaImageView.image = nil
            videoLayer.player = nil
            return
        }
        
        switch content.type {
        case .image:
            mediaImageView.hidden = false
            videoLayer.hidden = true
            
            if let image: UIImage = UIImage(contentsOfFile: content.URLString) {
                mediaImageView.image = image
            }
            
        case .video:
            videoLayer.hidden = false
            mediaImageView.hidden = true
            
            guard let videoURL: NSURL = NSURL(fileURLWithPath: content.URLString) else {
                return
            }
            
            guard let player: AVPlayer = AVPlayer(URL: videoURL) else {
                return
            }
            
            videoLayer.player = player
        }
        
        note.text = content.note
    }
    
    var media: ContentPresentable {
        get {
            switch content!.type {
            case .image:
                return mediaImageView
            case .video:
                return videoLayer
            }
        }
    }
}


// MARK : ContentContents

protocol ContainContents {
    var contents: [Content] { get }
    var numberOfContents: Int { get }
}

extension ContainContents {
    var contents: [Content] {
        return TimelineContentObject.shared.contents
    }
    
    var numberOfContents: Int {
        return contents.count
    }
}


// MARK : CanShowDetailView

protocol CanShowDetailView {
    
    func showDetailView(content: Content)
    var navigationController: UINavigationController? { get }
}

extension CanShowDetailView {
    func showDetailView(content: Content) {
        let detailViewController: DetailViewController = DetailViewController()
        detailViewController.content = content
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}


