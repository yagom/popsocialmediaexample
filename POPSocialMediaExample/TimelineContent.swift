//
//  TimelineContent.swift
//  POPSocialMediaExample
//
//  Created by JoSeongGyu on 2016. 7. 9..
//  Copyright © 2016년 yagom. All rights reserved.
//

import Foundation

class TimelineContentObject {
    static let shared: TimelineContentObject = TimelineContentObject()
    
    var contents: [Content]
    
    init() {
        var contentsArray: [Content] = [Content]()
        
        let notes: [String] = ["Let's go!", "야호", "Swift!!", "신나요"]
        
        
        for i in 0...3 {
            var content: Content?
            
            if let path: String = NSBundle.mainBundle().pathForResource("\(i)", ofType: "jpg") {
                content = Content(type: .image, URLString: path, note: notes[i])
            } else if let path: String = NSBundle.mainBundle().pathForResource("\(i)", ofType: "m4v") {
                content = Content(type: .video, URLString: path, note: notes[i])
            }
            
            if let content = content {
                contentsArray.append(content)
            }
        }
        self.contents = contentsArray
        print(self.contents.count)
    }
}
