//
//  TimelineTableViewController.swift
//  POPSocialMedia
//
//  Created by JoSeongGyu on 2016. 7. 8..
//  Copyright © 2016년 yagom. All rights reserved.
//

import UIKit

let timelineTitle = "Timeline"

class TimelineTableViewController: UITableViewController, ContainContents, CanShowDetailView {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = timelineTitle
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfContents
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let content: Content = contents[indexPath.row]
        
        let cell: TimelineTableViewCell
        
        let cellIdentifier = "reuseIdentifier"
            
        if let reuseCell: TimelineTableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? TimelineTableViewCell {
            cell = reuseCell
            cell.content = content
        } else {
            cell = TimelineTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier, content: content)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        showDetailView(contents[indexPath.row])
    }
}
