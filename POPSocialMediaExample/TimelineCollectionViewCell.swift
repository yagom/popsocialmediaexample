//
//  TimelineCollectionViewCell.swift
//  POPSocialMedia
//
//  Created by JoSeongGyu on 2016. 7. 8..
//  Copyright © 2016년 yagom. All rights reserved.
//

import UIKit
import AVFoundation


class TimelineCollectionViewCell: UICollectionViewCell, MediaContainer {
    
    lazy var videoLayer: AVPlayerLayer = AVPlayerLayer()
    lazy var mediaImageView: UIImageView = UIImageView()
    
    var note: UILabel = UILabel()
    
    var content: Content? {
        didSet {
            contentChanged()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.whiteColor()
        
        var mediaRect: CGRect = self.bounds
        mediaRect.size.width -= 10
        mediaRect.size.height -= 40
        mediaRect.origin.x += 5
        mediaRect.origin.y += 5
        
        mediaImageView.layout(in: mediaRect)
        videoLayer.layout(in: mediaRect)
        
        mediaImageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        self.addSubview(mediaImageView)
        self.layer.addSublayer(videoLayer)
        
        
            note.textAlignment = .Center
            
            var noteRect: CGRect = mediaRect
            noteRect.origin.y = self.bounds.size.height - 30
            noteRect.size.height = 30
            
            note.layout(in: noteRect)
            
            self.addSubview(note)
        
        
        note.font = UIFont.systemFontOfSize(15)
        note.adjustsFontSizeToFitWidth = true
        note.minimumScaleFactor = 0.1
    }
    
    convenience init(frame: CGRect, content: Content) {
        
        self.init(frame: frame)
        self.content = content
        contentChanged()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
