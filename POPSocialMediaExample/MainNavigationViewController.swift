//
//  MainNavigationViewController.swift
//  POPSocialMedia
//
//  Created by JoSeongGyu on 2016. 7. 7..
//  Copyright © 2016년 yagom. All rights reserved.
//

import UIKit

enum ListViewMode: String {
    case Table, Collection
}

class MainNavigationViewController: UINavigationController {

    private var listViewMode: ListViewMode = .Table {
        didSet {
            viewModeChanged()
        }
    }
    
    private var tableViewController: UITableViewController?
    private var collectionViewController: UICollectionViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewModeChanged()
    }
    
    // MARK: State Restoration
    private func viewModeChanged() {
        
        var rootViewController: UIViewController?
        
        switch listViewMode {
        case .Table:
            
            if let tableController = tableViewController {
                rootViewController = tableController
            } else {
                tableViewController = TimelineTableViewController()
            }
            
            rootViewController = tableViewController
            
        case .Collection:
            
            if let collcetionController = collectionViewController {
                rootViewController = collcetionController
            } else {
                var cellSize: CGSize = UIApplication.sharedApplication().keyWindow!.bounds.size
                
                cellSize.width /= 3.0
                cellSize.width -= 10
                
                cellSize.height = cellSize.width + 30
                
                let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                flowLayout.itemSize = cellSize
                
                collectionViewController = TimelineCollectionViewController(collectionViewLayout: flowLayout)
            }
            
            rootViewController = collectionViewController
        }
        
        guard let root: UIViewController = rootViewController else {
            return
        }
        
        let changeViewModeButton: UIBarButtonItem = UIBarButtonItem(title: "Mode", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.pivotViewMode))
        
        root.navigationItem.leftBarButtonItem = changeViewModeButton
        
        viewControllers = [root]
    }
    
    @objc private func pivotViewMode() {
        if listViewMode == .Table {
            listViewMode = .Collection
        } else {
            listViewMode = .Table
        }
    }
    
    

}
